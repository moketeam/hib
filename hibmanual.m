% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hibmanual class                                        %
% %                                                        %
% % Keysight M8195A internal synchronisation               %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe / Felix Gro�                             %
% % graefe@is.mpg.de / fgross@is.mpg.de                    %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hibmanual < hibsync
    %Public Properties
    properties (Access = public)
        Channels = 1
        Title = 'Manual Synchronization';
    end
    
    %Private Properties
    properties (Access = private)
        %specific properties of internal clock
        ClockFrequency = 100e6;
        %specific properties of M8195A
        Bitdepth = 4;
        DefaultSampleRate = 64e9;
        Prescaler = 32;
        %GUI handle storage
        syncEdit = [];
        syncChannels = [];
        syncBaseFrequency = [];
        syncSampleRate = [];
        syncSyncFrequency = [];
    end
    
    %Overload abstracted properties, calculated from other properties
    properties (Dependent = true)
        BaseFrequency
        SampleRate
        SyncFrequency
        Display
    end
    
    methods
        function BaseFrequency = get.BaseFrequency(obj)
            %calculates an Bitdepth approximation for the Base Frequency
            BaseFrequency = obj.ClockFrequency;
        end
        function SampleRate = get.SampleRate(obj)
            %calculates the actual Sample Rate based on the Base Frequency
            SampleRate = obj.DefaultSampleRate;
        end
        function SyncFrequency = get.SyncFrequency(obj)
            %sync frequency is adjusted for prescaler multiplier
            SyncFrequency = obj.ClockFrequency;
        end
        function Display = get.Display(obj)
            %display text for GUI
            Display = cell(2,1);
            Display{1} = ['Clock: ' num2str(obj.ClockFrequency/1e6) ' MHz'];
            Display{2} = ['Rate: ' num2str(obj.SampleRate/1e9) ' GS/s'];
        end
    end
    
    %GUI methods
    methods        
        function obj = drawGUI(obj, panel)
            %determine panel drawing area
            panelArea = panel.InnerPosition;

            %draw sync information
            %determine drawing constraints for info boxes
            PanelPos(1) = 5; %position left
            TopPos = panelArea(4); %position top
            PanelPos(3) = panelArea(3) - 2*5; %width
            PanelPos(4) = 20; %height
            
            %draw number of channels
            PanelPos(2) = TopPos - 20 - 5; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Synchronisation with Internal Clock', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom

        end
    end
end