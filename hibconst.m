% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hibconst class                                         %
% %                                                        %
% % generates constant signal                              %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe                                          %
% % graefe@is.mpg.de                                       %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hibconst < hibsignal
    %Public Access Properites and Set Properties
    properties (Access = public)
        MagicNumber = 1;
    end
    methods
        function obj = set.MagicNumber(obj, MagicNumber)
            %only accept integer Magic Numbers
            obj.MagicNumber = round(MagicNumber);
        end
    end
    
    %Dependent Output Properties
    properties (Dependent = true)
        Signal
        Display
    end
    methods
        function Signal = get.Signal(obj)
            %generate constant signal
            SignalLength = obj.Sync.SampleRate / obj.Sync.BaseFrequency;
            Signal = zeros(1, SignalLength);
            PatternLength = floor(SignalLength / obj.MagicNumber);
            Pattern = obj.Amplitude * ones(1, PatternLength);
            DelayLength = round(obj.Delay * obj.Sync.SampleRate);
            if DelayLength < PatternLength
                Pattern(1:DelayLength) = 0;
            else
                Pattern = 0;
            end
            %repeat pattern for magic number
            for i=1:obj.MagicNumber
                Signal((i-1)*PatternLength+1:i*PatternLength) = Pattern;
            end
        end
        function Display = get.Display(obj)
            %display text for GUI
            Display = ['Constant (Magic Number: ' num2str(obj.MagicNumber) ', Amplitude: ' num2str(obj.Amplitude) ', Delay: ' num2str(obj.Delay, '%10.3e') ' s)'];
        end
    end
    
    %Basic methods
    methods
        function obj = hibconst(varargin)
            %Constructor is inherited from hibsignal
            obj@hibsignal(varargin{:})
        end
        function obj = GUI(obj)
            Input = inputdlg({'Amplitude:', 'Magic Number:', 'Delay [s]:'}, 'HiB Constant Signal', [1 50], {num2str(obj.Amplitude), num2str(obj.MagicNumber), num2str(obj.Delay)});
            if ~isempty(Input)
                obj.Amplitude = eval(Input{1});
                obj.MagicNumber = eval(Input{2});
                obj.Delay = eval(Input{3});
            end
        end
    end
end