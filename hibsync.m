% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hibsync super class                                    %
% %                                                        %
% % abstract for AWG synchronistation                      %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe                                          %
% % graefe@is.mpg.de                                       %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% a HiB Sync class needs to provide at least these properties

classdef hibsync < handle
    %synchronisation are handles, i.e. changes are always global
    
    %Public Properties
    properties (Access = public, Abstract = true)
        SyncFrequency %AWG Sync Frequency
        BaseFrequency %Pattern Base Frequency
        SampleRate %AWG Sample Rate
        Display %Display for GUI
        Title %Title for Sync overview
    end
    
    %Publivc Events
    events
        updateSync
    end
end