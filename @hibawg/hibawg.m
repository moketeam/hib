% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hib AWG class                                          %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe                                          %
% % graefe@is.mpg.de                                       %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hibawg < handle
    %stores AWG configuration
    
    %Abstract Properties
    properties (Abstract = true)
        Channels %number of channels
        Title %display title
    end
    
    %Private Properties
    properties% (Access = private)
        %GUI handle storage
        outputClockSource = [];
        outputTriggerMode = [];
        outputInstrAddress = [];
        outputTestConnection = [];
        outputAmplitude = cell(0);
        outputOffset = cell(0);
        %awgConfig
        arbConfig = struct('clockSource', 'ExtRef', 'triggerMode', 'Continuous', 'connectionType', 'visa',...
            'fixedSampleRate', 0, 'defaultSampleRate', 64e9, 'maximumSampleRate', 65e9,...
            'minimumSampleRate', 54e9, 'minimumSegmentSize', 128, 'maximumSegmentSize', 256*1024,...
            'segmentGranularity', 128, 'maxSegmentNumber', 1, 'maximumModules', 4, 'visaAddr', 'TCPIP0::localhost::hislip0::INSTR');
    end
    methods
        function arbConfig = get.arbConfig(obj)
            
            obj.arbConfig.numChannels = obj.Channels;
            obj.arbConfig.model = ['M8195A_' num2str(obj.Channels) 'ch_256k'];
            switch obj.Channels
                case 2
                    obj.arbConfig.channelMask = [1 0 0 1];
                    if ~isfield(obj.arbConfig, 'amplitude')
                        obj.arbConfig.amplitude = [1 1];
                    end
                    if ~isfield(obj.arbConfig, 'offset')
                        obj.arbConfig.offset = [0 0];
                    end
                    
                case 4
                    obj.arbConfig.channelMask = [1 1 1 1];
                    if ~isfield(obj.arbConfig, 'amplitude')
                        obj.arbConfig.amplitude = [1 1 1 1];
                    end
                    if ~isfield(obj.arbConfig, 'offset')
                        obj.arbConfig.offset = [0 0 0 0];
                    end
            end
            
            arbConfig = obj.arbConfig;
        end
        
    end
    
    %Device Methods
    methods
        function download(obj, Signal, Sync)
            %check for AWG connection and channel number
            result = obj.testConnection;
            if result ~= 1
                return
            end
            
            %update sync frequency
            obj.arbConfig.clockFreq = Sync.SyncFrequency;
            obj.updateConfig
            %generate AWG output blocks
            BlockSize = obj.arbConfig.segmentGranularity;
            MaxBlock = obj.arbConfig.maximumSegmentSize;
            SignalLength = size(Signal, 2);
            OutSignal = zeros(4, SignalLength);
            switch size(Signal, 1)
                case 2
                    OutSignal(1,:) = Signal(1,:);
                    OutSignal(4,:) = Signal(2,:);
                case 4
                    OutSignal = Signal;
            end
            
            %repeat signal until it matches granularity
            while (mod(size(OutSignal,2),BlockSize) ~= 0) && (size(OutSignal,2) < MaxBlock)
                OutSignal(:,end+1:end+size(Signal,2)) = Signal;
            end

            %download signal into AWG
            if size(OutSignal,2) < MaxBlock
%                 channelMapping = [1 0 0 0 0 0 0 0; ...
%                 0 0 1 0 0 0 0 0; ...
%                 0 0 0 0 1 0 0 0; ...
%                 0 0 0 0 0 0 1 0];
%                 obj.iqdownload(OutSignal, Sync.SampleRate, obj.arbConfig, channelMapping);
                obj.downloadM8195A(OutSignal, Sync.SampleRate);
            else
                msgbox('Segment Too Long')
            end
        end
        
        function updateInstrAddress(obj, ~, ~)
            if ~strcmp(obj.arbConfig.visaAddr, obj.outputInstrAddress.String)
                obj.arbConfig.visaAddr = obj.outputInstrAddress.String;
                obj.outputTestConnection.BackgroundColor = 0.94*[1 1 1];
            end
        end
        
        function updateClockSource(obj, ~, ~)
            %update clock source
            obj.arbConfig.clockSource = obj.outputClockSource.String{obj.outputClockSource.Value};
            if strcmp(obj.arbConfig.clockSource, 'IntRef')
                obj.outputTriggerMode.Enable = 'on';
            else
                obj.outputTriggerMode.Enable = 'off';
                contVal = find(strcmp(obj.outputTriggerMode.String, 'Continuous'));
                obj.outputTriggerMode.Value = contVal;
                obj.arbConfig.triggerMode = obj.outputTriggerMode.String{contVal};
            end
        end
        
        function updateTriggerMode(obj, ~, ~)
            %update Trigger Mode
            obj.arbConfig.triggerMode = obj.outputTriggerMode.String{obj.outputTriggerMode.Value};
        end
        
        function [connected, f] = openConnection(obj, ~, ~)
            %check for connections
            connection = instrfind('RsrcName', obj.arbConfig.visaAddr);
            %if none are available open a new one
            if isempty(connection)
                try
                    f = visa('agilent', obj.arbConfig.visaAddr);
                catch
                    connected = 0;
                    f = [];
                    return
                end
            else
                f = connection(1);
                connected = 1;
            end
            
            
            if (~isempty(f) && strcmp(f.Status, 'closed'))
                try
                    f.OutputBufferSize = 20000000;
                    f.InputBufferSize = 12800000;
                    f.Timeout = 35;
                    fopen(f);
                    connected = 1;
                catch
                    connected = 0;
                    f = [];
                    return
                end
            end
        end
        
        function result = testConnection(obj, ~, ~)
            %test AWG connection
            obj.outputTestConnection.String = 'Testing...';
            obj.updateInstrAddress
            drawnow
            
            [connected, f] = obj.openConnection;
            
            colorFailed = [0.9882 0.3686 0.0118];
            colorWarn = [0.9882 0.8745 0.0118];
            colorSuccess = [0.2784 0.6784 0.3608];
            
            if ~connected
                obj.outputTestConnection.BackgroundColor = colorFailed;
                errordlg(sprintf(['Cannot connect to %s. ' ...
                    'Please verify that 1. the MATLAB Instrument Control Toolbox is installed 2. the given VISA adress is valid and 3. the Keysight Soft Front Panel is running. '...
                    'Otherwise, try to establish a connection via the Keysight Connection Expert first.'],...
                    obj.arbConfig.visaAddr), 'Error', 'replace');
                result = 0;
            else
                %check for AWG Model
                if contains(query(f, '*IDN?'), 'M8195A')
                    if contains(query(f, '*OPT?'), sprintf('%03d', obj.Channels))
                        obj.outputTestConnection.BackgroundColor = colorSuccess;
                        result = 1;
                    else
                        obj.outputTestConnection.BackgroundColor = colorWarn;
                        waitfor(warndlg('Wrong number of channels selected, please change AWG model!', 'HiB Control Software', 'help'))
                        result = -1;
                    end
                else
                    obj.outputTestConnection.BackgroundColor = colorFailed;
                    waitfor(errordlg('Connection failed!', 'HiB Control Software', 'error'))
                    result = 0;
                end
            end
            obj.outputTestConnection.String = 'Test Connection';
        end
                
    end
    
    
    %GUI methods
    methods
        function obj = drawGUI(obj, panel)
            %determine panel drawing area
            panelArea = panel.InnerPosition;
            
            %draw configure button
            PanelPos(1) = 5; %position left
            PanelPos(2) = panelArea(4) - 20 ; %position bottom
            PanelPos(3) = panelArea(3) - 2*5; %width
            PanelPos(4) = 20; %height
            
            %draw clock source
            uicontrol(panel, 'Style', 'text', 'String', 'Clock Source', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            
            clockString = {'Unchanged', 'IntRef', 'AxieRef', 'ExtRef'};
            obj.outputClockSource = uicontrol(panel, 'Style', 'popupmenu', ...
                'String', clockString, 'value', find(ismember(clockString,obj.arbConfig.clockSource)),...
                'Units', 'Pixels', 'Position', PanelPos, 'Enable', 'off', 'Callback', @obj.updateClockSource);
            
            PanelPos(2) = PanelPos(2) - 20 - 5; %position bottom
            
            %draw trigger mode
            uicontrol(panel, 'Style', 'text', 'String', 'Trigger Mode', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            
            triggerString = {'None', 'Continuous', 'Triggered', 'Gated'};
            obj.outputTriggerMode = uicontrol(panel, 'Style', 'popupmenu', ...
                'String', triggerString, 'value', find(ismember(triggerString,obj.arbConfig.triggerMode)),...
                'Units', 'Pixels', 'Position', PanelPos, 'Enable', 'off', 'Callback', @obj.updateTriggerMode);
            
            PanelPos(2) = PanelPos(2) - 20 - 5; %position bottom
            
            %draw instrument address
            PanelPos(2) = PanelPos(2) - 10; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Instrument Address', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            obj.outputInstrAddress = uicontrol(panel, 'Style', 'edit', ...
                'String', obj.arbConfig.visaAddr, ...
                'Units', 'Pixels', 'Position', PanelPos, 'Callback', @obj.testConnection);
            PanelPos(2) = PanelPos(2) - 20 - 5; %position bottom
            obj.outputTestConnection = uicontrol(panel, 'Style', 'pushbutton', ...
                'String', 'Test Connection', 'Units', 'Pixels', ...
                'Position', PanelPos, 'Callback', @obj.testConnection);
            
            %draw output amplitude
            PanelPos(2) = PanelPos(2) - 20 - 20; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Amplitude [V]', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            SubPanelPos = PanelPos;
            SubPanelPos(3) = PanelPos(3)/obj.Channels - 5;
            for i=1:obj.Channels
                obj.outputAmplitude{i} = uicontrol(panel, 'Style', 'edit', ...
                    'Units', 'Pixels', 'Position', SubPanelPos);
                try
                    obj.outputAmplitude{i}.String = num2str(obj.arbConfig.amplitude(i));
                catch
                    obj.outputAmplitude{i}.String = num2str(mean(obj.arbConfig.amplitude));
                end
                SubPanelPos(1) = SubPanelPos(1) + SubPanelPos(3) + 5;
            end
            
            %draw output offset
            PanelPos(2) = PanelPos(2) - 20 - 10; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Offset [V]', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            SubPanelPos = PanelPos;
            SubPanelPos(3) = PanelPos(3)/obj.Channels - 5;
            for i=1:obj.Channels
                obj.outputOffset{i} = uicontrol(panel, 'Style', 'edit', ...
                    'Units', 'Pixels', 'Position', SubPanelPos);
                try
                    obj.outputOffset{i}.String = num2str(obj.arbConfig.offset(i));
                catch
                    obj.outputOffset{i}.String = num2str(mean(obj.arbConfig.offset));
                end
                SubPanelPos(1) = SubPanelPos(1) + SubPanelPos(3) + 5;
            end
        end
        
        function updateConfig(obj, ~, ~)
            %update config from GUI
            obj.updateInstrAddress
            obj.arbConfig.amplitude = [];
            obj.arbConfig.offset = [];
            for i=1:obj.Channels
                obj.arbConfig.amplitude(i) = str2double(obj.outputAmplitude{i}.String);
                obj.arbConfig.offset(i) = str2double(obj.outputOffset{i}.String);
            end
        end
        
        function updateDisplay(obj, ~, ~)
            %update display from config
            obj.outputInstrAddress.String = obj.arbConfig.visaAddr;
            for i=1:obj.Channels
                obj.outputAmplitude{i}.String = num2str(obj.arbConfig.amplitude(i));
                obj.outputOffset{i}.String = num2str(obj.arbConfig.offset(i));
            end
        end
    end
end