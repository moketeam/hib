function downloadM8195A(obj, data, sampleRate)

% open connection to AWG
[connected, f] = obj.openConnection;
if ~connected
    errordlg('Not able to communicate with instrument. Please check VISA address.');
    error('Not able to communicate with instrument. Please check VISA address.');
end

cmd = '';
switch (obj.arbConfig.clockSource)
    case 'Unchanged'
        % nothing to do
    case 'IntRef'
        cmd = sprintf(':ROSC:SOURce INT; ');
    case 'AxieRef'
        cmd = sprintf(':ROSC:SOURce AXI; ');
    case 'ExtRef'
        cmd = sprintf(':ROSC:SOURce EXT; :ROSC:FREQuency %.15g; ', obj.arbConfig.clockFreq);
end

awgfprintf(f, sprintf('%s:FREQuency:RASTer %.15g;', cmd, sampleRate));

% set trigger mode
switch(obj.arbConfig.triggerMode)
    case 'Continuous'
        contMode = 1;
        gateMode = 0;
    case 'Triggered'
        contMode = 0;
        gateMode = 0;
    case 'Gated'
        contMode = 0;
        gateMode = 1;
    otherwise
        contMode = -1;
        gateMode = -1;
end

if (contMode >= 0)
    awgfprintf(f, sprintf(':INIT:CONT %d;GATE %d', contMode, gateMode));
end


% define the segment number
segm_num = 1;
% stop waveform generation
awgfprintf(f, ':ABORT');
% delete all segments (you might not want to do this every time)
awgfprintf(f, ':TRACE:DEL:ALL');
% scale the waveform to 8 bit integers
dacValues = int8(round(127 * data));


fsDivider = 1;
if obj.Channels == 4
    channels = 1:4;
    awgfprintf(f, sprintf(':INST:DACM FOUR;:TRAC1:MMOD INT;:TRAC2:MMOD INT;:TRAC3:MMOD INT;:TRAC4:MMOD INT;:INST:MEM:EXT:RDIV DIV%d', fsDivider));
elseif obj.Channels == 2
    channels = [1 4];
    awgfprintf(f, sprintf(':INST:DACM DUAL;:TRAC1:MMOD INT;:TRAC4:MMOD INT;:INST:MEM:EXT:RDIV DIV%d', fsDivider));
end

% define a waveform segment. Must be a multiple of 128 samples
awgfprintf(f, sprintf(':TRACE:DEFINE %d,%d', 1, size(data,2)));

for i = channels
    % download to M8195A
    cmd = sprintf(':TRACE%d:DATA %d,%d,', i, segm_num, 0);
    binblockwrite(f, dacValues(i,:), 'int8', cmd);
    
    % select the segment
    awgfprintf(f, sprintf(':TRACE%d:SELECT %d', i, segm_num));

    amplitude = obj.arbConfig.amplitude;
    offset = obj.arbConfig.offset;
    if obj.Channels == 2
        amplitude(4) = amplitude(2);
        offset(4) = offset(2);
        amplitude(2:3) = 1;
        offset(2:3) = 0;
    end
    awgfprintf(f, sprintf(':VOLTage%d:AMPLitude %g', i, amplitude(i)));
    awgfprintf(f, sprintf(':VOLTage%d:OFFSet %g', i, offset(i)));
    
    awgfprintf(f, sprintf(':OUTPUT%d ON', i));

end

% start waveform generation
awgfprintf(f, sprintf(':INIT:IMM'));

% close connection to AWG
fclose(f);
delete(f);
end



function awgfprintf(f, cmd)
    %flush old errors
    while ~strcmp(query(f, ':syst:err?'),['0,"No error"' newline])
    end
    
    %write command
    fprintf(f, cmd);
    
    %check for new errors
    answer = query(f, ':syst:err?');
    if ~strcmp(answer,['0,"No error"' newline])
        errordlg(answer)
        %error(answer)
    end    
end