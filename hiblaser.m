% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hiblaser class                                         %
% %                                                        %
% % Keysight M8195A synchronisation for Lab Laser          %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe                                          %
% % graefe@is.mpg.de                                       %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hiblaser < hibsync
    %Public Properties
    properties (Access = public)
        Channels = 1; %number of detection channels / time steps
        Title = 'Femto FErb 780';
    end
    
    %Private Properties
    properties (Access = private)
        %specific properties of Laser
        LaserFrequency = 100e6;
        %specific properties of M8195A
        Bitdepth = 8;
        DefaultSampleRate = 64e9;
        Prescaler = 32;
        MinSyncFrequency = 10e6;
        %GUI handle storage
        syncInfo = [];
        syncBaseFrequency = [];
        syncSampleRate = [];
        syncSyncFrequency = [];
    end
    
    %Intialization Method
    methods
        function obj = hiblaser
            %nothing to init
        end
    end
    
    %Overload abstracted properties, calculated from other properties
    properties (Dependent = true)
        BaseFrequency
        SampleRate
        SyncFrequency
        Display
    end
    methods
        function BaseFrequency = get.BaseFrequency(obj)
            %without super nyquist this is the laser frequency
            BaseFrequency = obj.LaserFrequency;
        end
        function SampleRate = get.SampleRate(obj)
            %without super nyquist this is the default sampel rate
            SampleRate = obj.DefaultSampleRate;
        end
        function SyncFrequency = get.SyncFrequency(obj)
            %without super nyquist this is the laser frequency
            SyncFrequency = obj.LaserFrequency;
        end
        function Display = get.Display(obj)
            %display text for GUI
            Display = cell(2,1);
            Display{1} = ['Sync: ' num2str(obj.SyncFrequency/1e6) ' MHz'];
            Display{2} = ['Rate: ' num2str(obj.SampleRate/1e9) ' GS/s'];
        end
    end
    
    %GUI methods
    methods
        function obj = GUI(obj, ~, ~)
            msgbox('Synchronization for 100 MHz Laser', 'HiB Laser Sync');
        end
        
        function obj = drawGUI(obj, panel)
            %determine panel drawing area
            panelArea = panel.InnerPosition;
            
            %draw sync info button
            PanelPos(1) = 5; %position left
            PanelPos(2) = 5; %position bottom
            PanelPos(3) = panelArea(3) - 2*5; %width
            PanelPos(4) = 20; %height
            obj.syncInfo = uicontrol(panel, 'Style', 'pushbutton', ...
                'String', 'Synchronisation Info', 'Units', 'Pixels', ...c
                'Position', PanelPos, 'Callback', @obj.GUI);
            
            %draw sync information
            %determine drawing constraints for info boxes
            PanelPos(1) = 5; %position left
            TopPos = panelArea(4); %position top
            PanelPos(3) = panelArea(3) - 2*5; %width
            PanelPos(4) = 20; %height
            
            %draw base frequency
            PanelPos(2) = TopPos - 20 - 5; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Base Frequency', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            obj.syncBaseFrequency = uicontrol(panel, 'Style', 'edit', ...
                'String', [num2str(obj.BaseFrequency/1e6), ' MHz'], 'Enable', 'inactive', ...
                'Units', 'Pixels', 'Position', PanelPos);
            %draw sample rate
            PanelPos(2) = PanelPos(2) - 20 - 10; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Sample Rate', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            obj.syncSampleRate = uicontrol(panel, 'Style', 'edit', ...
                'String', [num2str(obj.SampleRate/1e9), ' GS/s'], 'Enable', 'inactive', ...
                'Units', 'Pixels', 'Position', PanelPos);
            %draw sync frequency
            PanelPos(2) = PanelPos(2) - 20 - 10; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Synchronisation Frequency', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            obj.syncSyncFrequency = uicontrol(panel, 'Style', 'edit', ...
                'String', [num2str(obj.SyncFrequency/1e6), ' MHz'], 'Enable', 'inactive', ...
                'Units', 'Pixels', 'Position', PanelPos);
        end
    end
end