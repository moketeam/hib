% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hibsin class                                           %
% %                                                        %
% % generates sine signals                                 %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe                                          %
% % graefe@is.mpg.de                                       %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hibsin < hibsignal
    %Dependent Output Properties
    properties (Dependent = true)
        Signal
        MagicNumber
        Frequency
        Display
    end
    methods
        function Signal = get.Signal(obj)
            %calculate sine signal
            SignalLength = obj.Sync.SampleRate / obj.Sync.BaseFrequency;
            Period = (SignalLength / obj.MagicNumber) / (2*pi);
            Phase = mod(obj.Waveform.Phase/360 * Period, Period) * (2*pi);
            Time = 1:SignalLength;
            Signal = obj.Amplitude * sin((Time + Phase) / Period);
        end
        function MagicNumber = get.MagicNumber(obj)
            %calculate magic number from requested frequency
            if isempty(obj.Sync) || isempty(obj.Waveform.Frequency)
                %return empty if required properties are not set
                MagicNumber = [];
            else
                MagicNumber = round(obj.Waveform.Frequency / obj.Sync.BaseFrequency);
                if MagicNumber == 0
                    MagicNumber = 1;
                end
            end
        end
        function Frequency = get.Frequency(obj)
            %calculate output frequency from magic number
            if isempty(obj.MagicNumber)
                Frequency = [];
            else
                Frequency = obj.Sync.BaseFrequency * obj.MagicNumber;
            end
        end
        function Display = get.Display(obj)
            %display text for GUI
            Display = ['Sine (Magic Number: ' num2str(obj.MagicNumber) ', Frequency: ' num2str(obj.Frequency, '%10.3e') ' Hz, Phase: ' num2str(obj.Waveform.Phase) '�, Amplitude: ' num2str(obj.Amplitude) ')'];
        end
    end
    
    %Basic methods
    methods
        function obj = hibsin(varargin)
            %Constructor is inherited from hibsignal
            obj@hibsignal(varargin{:})
            obj.Delay = [];
            obj.Waveform = struct('Frequency', [], 'Phase', 0);
        end
        function obj = GUI(obj)
            Input = inputdlg({'Freqeuncy [Hz]:', 'Phase [�]:', 'Amplitude:'}, 'HiB Sine Signal', [1 50], {num2str(obj.Waveform.Frequency), num2str(obj.Waveform.Phase), num2str(obj.Amplitude)});
            if ~isempty(Input)
                obj.Waveform.Frequency = eval(Input{1});
                obj.Waveform.Phase = eval(Input{2});
                obj.Amplitude = eval(Input{3});
            end
        end
    end
end