% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hibmaxymus class                                       %
% %                                                        %
% % Keysight M8195A synchronisation at MAXYMUS             %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe                                          %
% % graefe@is.mpg.de                                       %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hibmaxymus < hibsync
    %Public Properties
    properties (Access = public)
        Channels = 1; %number of detection channels / time steps
        Title = 'MAXYMUS AD9914';
    end
    
    %Private Properties
    properties (Access = private)
        %specific properties of BESSY
        SynchrotronFrequency = 500e6;
        %specific properties of M8195A
        Bitdepth = 4;
        DefaultSampleRate = 64e9;
        Prescaler = 32;
        MinSyncFrequency = 10e6;
        %specific properties of AD9914
        ComPort = [];
        serialObject = [];
        timeOut = 2;
        %GUI handle storage
        syncEdit = [];
        syncCOM = [];
        syncWriteAD = [];
        syncChannels = [];
        syncBaseFrequency = [];
        syncSampleRate = [];
        syncSyncFrequency = [];
        syncMN = [];
    end
    
    %Intialization Method
    methods
        function obj = hibmaxymus(varargin)
            %optional input: Channels
            if nargin > 0
                obj.Channels = varargin{1};
            end
            ComPorts = seriallist;
            if ~isempty(seriallist)
                obj.ComPort = ComPorts(1);
                obj.serialObject = serial(obj.ComPort, 'BaudRate', 9600);
            end
        end
    end
    
    %Overload abstracted properties, calculated from other properties
    properties (Dependent = true)
        BaseFrequency
        SampleRate
        SyncFrequency
        Display
    end
    methods
        function BaseFrequency = get.BaseFrequency(obj)
            %calculates an Bitdepth approximation for the Base Frequency
            BaseFrequency = obj.SyncFrequency / obj.M;
        end
        function SampleRate = get.SampleRate(obj)
            %calculates the actual Sample Rate based on the Base Frequency
            FracDiv = round(obj.DefaultSampleRate / obj.SyncFrequency / obj.Prescaler);
            SampleRate = obj.SyncFrequency * obj.Prescaler*FracDiv;
        end
        function SyncFrequency = get.SyncFrequency(obj)
            %sync frequency is adjusted for prescaler multiplier
            SyncFrequency = round(obj.SynchrotronFrequency * obj.M / obj.Channels * 2^obj.Bitdepth / 1e6) / 2^obj.Bitdepth * 1e6;
        end
        function Display = get.Display(obj)
            %display text for GUI
            Display = cell(5,1);
            Display{1} = [num2str(obj.Channels) ' Channels'];
            Display{2} = ['Base: ' num2str(obj.SyncFrequency/1e6) ' MHz'];
            Display{3} = ['Rate: ' num2str(obj.SampleRate/1e9) ' GS/s'];
            Display{4} = ['Sync: ' num2str(obj.SyncFrequency/1e6) ' MHz'];
            Display{5} = ['M/N: ' num2str(obj.M) '/' num2str(obj.N)];
        end
    end
    
    %GUI methods
    methods
        function obj = GUI(obj, ~, ~)
            Input = inputdlg({'Channels:'}, 'HiB MAXYMUS Sync', [1 50], {num2str(obj.Channels)});
            if ~isempty(Input)
                obj.Channels = eval(Input{1});
                obj.syncChannels.String = num2str(obj.Channels);
                obj.syncBaseFrequency.String = [num2str(obj.BaseFrequency/1e6), ' MHz'];
                obj.syncSampleRate.String = [num2str(obj.SampleRate/1e9), ' GS/s'];
                obj.syncSyncFrequency.String = [num2str(obj.SyncFrequency/1e6), ' MHz'];
                obj.syncMN.String = [num2str(obj.M), '/', num2str(obj.N)];
                notify(obj, 'updateSync')
            end
        end
        
        function obj = GUIchangeCOM(obj, ~, ~)
            obj.ComPort = obj.syncCOM.String;
        end
        
        function obj = drawGUI(obj, panel)
            %determine panel drawing area
            panelArea = panel.InnerPosition;
            
            %draw update AD9914 button
            PanelPos(1) = 5; %position left
            PanelPos(2) = 5; %position bottom
            PanelPos(3) = panelArea(3) - 2*5; %width
            PanelPos(4) = 20; %height
            obj.syncWriteAD = uicontrol(panel, 'Style', 'pushbutton', ...
                'String', 'Update AD9914', 'Units', 'Pixels', ...
                'Position', PanelPos, 'Callback', @obj.updateAD9914, ...
                'Enable', 'off');
            
            %draw COM port selector
            PanelPos(1) = 5; %position left
            PanelPos(2) = 20 + 2*5; %position bottom
            PanelPos(3) = panelArea(3) - 2*5; %width
            PanelPos(4) = 20; %height
            obj.syncCOM = uicontrol(panel, 'Style', 'popupmenu', ...
                'String', 'no COM port', 'Units', 'Pixels', ...
                'Position', PanelPos, 'Callback', @obj.GUIchangeCOM, ...
                'Enable', 'off' );
                
            if ~isempty(seriallist)    
                obj.syncCOM.String = seriallist;
                obj.syncCOM.Enable = 'on';
                obj.syncWriteAD.Enable = 'on';
            end
            
            %draw sync edit button
            PanelPos(1) = 5; %position left
            PanelPos(2) = 2*20 + 3*5; %position bottom
            PanelPos(3) = panelArea(3) - 2*5; %width
            PanelPos(4) = 20; %height
            obj.syncEdit = uicontrol(panel, 'Style', 'pushbutton', ...
                'String', 'Edit Synchronisation', 'Units', 'Pixels', ...
                'Position', PanelPos, 'Callback', @obj.GUI);
            
            %draw sync information
            %determine drawing constraints for info boxes
            PanelPos(1) = 5; %position left
            TopPos = panelArea(4); %position top
            PanelPos(3) = panelArea(3) - 2*5; %width
            PanelPos(4) = 20; %height
            
            %draw number of channels
            PanelPos(2) = TopPos - 20 - 5; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Channels', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            obj.syncChannels = uicontrol(panel, 'Style', 'edit', ...
                'String', num2str(obj.Channels), 'Enable', 'inactive', ...
                'Units', 'Pixels', 'Position', PanelPos);
            %draw base frequency
            PanelPos(2) = PanelPos(2) - 20 - 10; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Base Frequency', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            obj.syncBaseFrequency = uicontrol(panel, 'Style', 'edit', ...
                'String', [num2str(obj.BaseFrequency/1e6), ' MHz'], 'Enable', 'inactive', ...
                'Units', 'Pixels', 'Position', PanelPos);
            %draw sample rate
            PanelPos(2) = PanelPos(2) - 20 - 10; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Sample Rate', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            obj.syncSampleRate = uicontrol(panel, 'Style', 'edit', ...
                'String', [num2str(obj.SampleRate/1e9), ' GS/s'], 'Enable', 'inactive', ...
                'Units', 'Pixels', 'Position', PanelPos);
            %draw sync frequency
            PanelPos(2) = PanelPos(2) - 20 - 10; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Synchronisation Frequency', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            obj.syncSyncFrequency = uicontrol(panel, 'Style', 'edit', ...
                'String', [num2str(obj.SyncFrequency/1e6), ' MHz'], 'Enable', 'inactive', ...
                'Units', 'Pixels', 'Position', PanelPos);
            %draw M/N
            PanelPos(2) = PanelPos(2) - 20 - 10; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'M/N', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            obj.syncMN = uicontrol(panel, 'Style', 'edit', ...
                'String', [num2str(obj.M), '/', num2str(obj.N)], 'Enable', 'inactive', ...
                'Units', 'Pixels', 'Position', PanelPos);
        end
    end
    
    %AD9914 prescaler specific functions
    properties (Dependent = true)
        M %prescaler M/N numerator
        N %prescaler M/N denominator
    end
    methods
        function M = get.M(obj)
            M = ceil((obj.MinSyncFrequency) / obj.SynchrotronFrequency * obj.Channels);
            if M > 1
                M = M + mod(M, 2); %only use even multipliers
            end
        end
        function N = get.N(obj)
            N = obj.Channels;
        end
    end
    methods
        function obj = updateAD9914(obj, ~, ~)
            try
                %write M and N values to AD9914 via Arudino Booster
                fopen(obj.serialObject);
                pause(obj.timeOut)
                queryString = char(rot90(fread(obj.serialObject, obj.serialObject.BytesAvailable)));
                disp(queryString)
                commandString = ['SMN,' num2str(obj.M) ',' num2str(obj.N*7)];
                fwrite(obj.serialObject, commandString);
                pause(obj.timeOut)
                queryString = char(rot90(fread(obj.serialObject, obj.serialObject.BytesAvailable)));
                disp(queryString)
                fclose(obj.serialObject);
            catch
                fclose(obj.serialObject);
                msgbox('Failed to communicate with AD 9914', 'HiB Control', 'error');
            end
        end
    end
end