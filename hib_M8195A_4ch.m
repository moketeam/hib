% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hib Keysight M8195A 2ch class                          %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe                                          %
% % graefe@is.mpg.de                                       %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hib_M8195A_4ch < hibawg
    %Public Properties
    properties (Access = public)
        Channels = 4; %number of channels
        Title = 'Keysight M8195A 4ch';
    end
end