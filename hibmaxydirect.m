    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hibmaxymus class                                       %
% %                                                        %
% % Keysight M8195A synchronisation at MAXYMUS             %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe                                          %
% % graefe@is.mpg.de                                       %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hibmaxydirect < hibsync
    %Public Properties
    properties (Access = public)
        Channels = 1; %number of detection channels / time steps
        Title = 'MAXYMUS direct';
    end
    
    %Private Properties
    properties (Access = private)
        %specific properties of BESSY
        SynchrotronFrequency = 500e6;
        %specific properties of M8195A
        Bitdepth = 4;
        DefaultSampleRate = 64e9;
        Prescaler = 32;
        %GUI handle storage
        syncEdit = [];
        syncChannels = [];
        syncBaseFrequency = [];
        syncSampleRate = [];
        syncSyncFrequency = [];
    end
    
    %Intialization Method
    methods
        function obj = hibmaxydirect(varargin)
            %optional input: Channels
            if nargin > 0
                obj.Channels = varargin{1};
            end
        end
    end
    
    %Overload abstracted properties, calculated from other properties
    properties (Dependent = true)
        BaseFrequency
        SampleRate
        SyncFrequency
        Display
    end
    methods
        function BaseFrequency = get.BaseFrequency(obj)
            %calculates an Bitdepth approximation for the Base Frequency
            BaseFrequency = round(obj.SynchrotronFrequency / obj.Channels * 2^obj.Bitdepth / 1e6) / 2^obj.Bitdepth * 1e6;
        end
        function SampleRate = get.SampleRate(obj)
            %calculates the actual Sample Rate based on the Base Frequency
            FracDiv = round(obj.DefaultSampleRate / obj.BaseFrequency / obj.Prescaler);
            SampleRate = obj.BaseFrequency * obj.Prescaler*FracDiv;
        end
        function SyncFrequency = get.SyncFrequency(obj)
            %sync frequency is adjusted for prescaler multiplier
            SyncFrequency = obj.SynchrotronFrequency;
        end
        function Display = get.Display(obj)
            %display text for GUI
            Display = cell(4,1);
            Display{1} = [num2str(obj.Channels) ' Channels'];
            Display{2} = ['Sync: ' num2str(obj.SyncFrequency/1e6) ' MHz'];
            Display{3} = ['Rate: ' num2str(obj.SampleRate/1e9) ' GS/s'];
            Display{4} = ['Base: ' num2str(obj.BaseFrequency/1e6) ' MHz'];
        end
    end
    
    %GUI methods
    methods
        function obj = GUI(obj, ~, ~)
            Input = inputdlg({'Channels:'}, 'HiB MAXYMUS Direct Sync', [1 50], {num2str(obj.Channels)});
            if ~isempty(Input)
                obj.Channels = eval(Input{1});
                obj.syncChannels.String = num2str(obj.Channels);
                obj.syncBaseFrequency.String = [num2str(obj.BaseFrequency/1e6), ' MHz'];
                obj.syncSampleRate.String = [num2str(obj.SampleRate/1e9), ' GS/s'];
                obj.syncSyncFrequency.String = [num2str(obj.SyncFrequency/1e6), ' MHz'];
                notify(obj, 'updateSync')
            end
        end
        
        function obj = drawGUI(obj, panel)
            %determine panel drawing area
            panelArea = panel.InnerPosition;
            
            %draw sync edit button
            PanelPos(1) = 5; %position left
            PanelPos(2) = 5; %position bottom
            PanelPos(3) = panelArea(3) - 2*5; %width
            PanelPos(4) = 20; %height
            obj.syncEdit = uicontrol(panel, 'Style', 'pushbutton', ...
                'String', 'Edit Synchronisation', 'Units', 'Pixels', ...c
                'Position', PanelPos, 'Callback', @obj.GUI);
            
            %draw sync information
            %determine drawing constraints for info boxes
            PanelPos(1) = 5; %position left
            TopPos = panelArea(4); %position top
            PanelPos(3) = panelArea(3) - 2*5; %width
            PanelPos(4) = 20; %height
            
            %draw number of channels
            PanelPos(2) = TopPos - 20 - 5; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Channels', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            obj.syncChannels = uicontrol(panel, 'Style', 'edit', ...
                'String', num2str(obj.Channels), 'Enable', 'inactive', ...
                'Units', 'Pixels', 'Position', PanelPos);
            %draw base frequency
            PanelPos(2) = PanelPos(2) - 20 - 10; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Base Frequency', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            obj.syncBaseFrequency = uicontrol(panel, 'Style', 'edit', ...
                'String', [num2str(obj.BaseFrequency/1e6), ' MHz'], 'Enable', 'inactive', ...
                'Units', 'Pixels', 'Position', PanelPos);
            %draw sample rate
            PanelPos(2) = PanelPos(2) - 20 - 10; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Sample Rate', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            obj.syncSampleRate = uicontrol(panel, 'Style', 'edit', ...
                'String', [num2str(obj.SampleRate/1e9), ' GS/s'], 'Enable', 'inactive', ...
                'Units', 'Pixels', 'Position', PanelPos);
            %draw sync frequency
            PanelPos(2) = PanelPos(2) - 20 - 10; %position bottom
            uicontrol(panel, 'Style', 'text', 'String', 'Synchronisation Frequency', ...
                'Units', 'Pixels', 'Position', PanelPos);
            PanelPos(2) = PanelPos(2) - 20; %position bottom
            obj.syncSyncFrequency = uicontrol(panel, 'Style', 'edit', ...
                'String', [num2str(obj.SyncFrequency/1e6), ' MHz'], 'Enable', 'inactive', ...
                'Units', 'Pixels', 'Position', PanelPos);
        end
    end
end