% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hibsignal super class                                  %
% %                                                        %
% % abstract for signals                                   %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe                                          %
% % graefe@is.mpg.de                                       %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hibsignal
    %Public Properties and Set Methods
    properties (Access = public)
        %Waveform Properties
        Amplitude = 1;
        Delay = 0;
        Waveform = struct();
        %Synchronisation Properties
        Sync = [];
    end
    methods
        function obj = set.Amplitude(obj, Amplitude)
            %make sure that Amplitude <= 1
            if abs(Amplitude) > 1
                Amplitude = Amplitude/Amplitude;
            end
            obj.Amplitude = Amplitude;
        end
    end
    
    %Basic Methods
    methods
        function obj = hibsignal(varargin)
            %constructor
            %optional input: hibsync
            if nargin > 0
                obj.Sync = varargin{1};
            end
        end
        
        function GUI(~)
            %no GUI implementation info
            msgbox('There is no GUI for this signal!', 'HiB Signal', 'help')
        end
    end
end