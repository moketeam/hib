% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hibconst class                                         %
% %                                                        %
% % generates constant signal                              %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe                                          %
% % graefe@is.mpg.de                                       %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hibempty < hibsignal
    %Public Access Properites and Set Properties
    properties (Access = public)
        MagicNumber = 1;
    end
    methods
        function obj = set.MagicNumber(obj, MagicNumber)
            %only accept integer Magic Numbers
            obj.MagicNumber = round(MagicNumber);
        end
        function MagicNumber = get.MagicNumber(obj)
            %make sure that magic number is never empty
            MagicNumber = obj.MagicNumber;
            if isempty(MagicNumber)
                MagicNumber = 1;
            end
        end
    end
    
    %Dependent Output Properties
    properties (Dependent = true)
        Signal
        Display
    end
    methods
        function Signal = get.Signal(obj)
            %generate constant signal
            SignalLength = obj.Sync.SampleRate / obj.Sync.BaseFrequency;
            Signal = zeros(1, SignalLength);
            PatternLength = floor(SignalLength / obj.MagicNumber);
            Pattern = zeros(1, PatternLength);
            %repeat pattern for magic number
            for i=1:obj.MagicNumber
                Signal((i-1)*PatternLength+1:i*PatternLength) = Pattern;
            end
        end
    end
    
    %Basic methods
    methods
        function obj = hibempty(varargin)
            %Constructor is inherited from hibsignal
            obj@hibsignal(varargin{:})
        end
    end
end