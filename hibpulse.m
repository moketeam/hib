% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hibpulse class                                         %
% %                                                        %
% % generates sine signals                                 %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe                                          %
% % graefe@is.mpg.de                                       %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hibpulse < hibsignal
    %Public Access Properites and Set Properties
    properties (Access = public)
        MagicNumber = 1;
    end
    methods
        function obj = set.MagicNumber(obj, MagicNumber)
            %only accept integer Magic Numbers
            obj.MagicNumber = round(MagicNumber);
        end
    end
    
    %Dependent Output Properties
    properties (Dependent = true)
        Signal
        Display
    end
    methods
        function Signal = get.Signal(obj)
            %generate constant signal
            SignalLength = obj.Sync.SampleRate / obj.Sync.BaseFrequency;
            Signal = zeros(1, SignalLength);
            PatternLength = floor(SignalLength / obj.MagicNumber);
            Pattern = zeros(1, PatternLength);
            DelayLength = round(obj.Delay * obj.Sync.SampleRate);
            PulseLength = round(obj.Waveform.Length * obj.Sync.SampleRate);
            %if delay is longer than pattern everything becomes zero
            if DelayLength < PatternLength
                PulseStart = DelayLength + 1;
                %if pulse is longer than pattern, it will be croped
                if (PulseStart + PulseLength) > PatternLength
                    PulseEnd = PatternLength;
                else
                    PulseEnd = PulseStart + PulseLength;
                end
                Pattern(PulseStart:PulseEnd) = obj.Amplitude;
            end
            %repeat pattern for magic number
            IdealPatternLength = SignalLength / obj.MagicNumber;
            for i=1:obj.MagicNumber
                PatternStart = round((i-1)*IdealPatternLength+1);
                PatternEnd = PatternStart+PatternLength-1;
                if PatternEnd > SignalLength
                    PatternStart = SignalLength-PatternLength;
                end
                Signal(PatternStart:PatternEnd) = Pattern;
            end
        end
        function Display = get.Display(obj)
            %display text for GUI
            Display = ['Pulse (Magic Number: ' num2str(obj.MagicNumber) ', Length: ' num2str(obj.Waveform.Length, '%10.3e') ' s, Amplitude: ' num2str(obj.Amplitude) ', Delay: ' num2str(obj.Delay, '%10.3e') ' s)'];
        end
    end
    
    %Basic methods
    methods
        function obj = hibpulse(varargin)
            %Constructor is inherited from hibsignal
            obj@hibsignal(varargin{:})
            obj.Delay = 0;
            obj.Waveform = struct('Length', []);
        end
        function obj = GUI(obj)
            Input = inputdlg({'Pulse Length [s]:', 'Amplitude:', 'Magic Number:', 'Delay [s]:'}, 'HiB Constant Signal', [1 50], {num2str(obj.Waveform.Length), num2str(obj.Amplitude), num2str(obj.MagicNumber), num2str(obj.Delay)});
            if ~isempty(Input)
                obj.Waveform.Length = eval(Input{1});
                obj.Amplitude = eval(Input{2});
                obj.MagicNumber = eval(Input{3});
                obj.Delay = eval(Input{4});
            end
        end
    end
end