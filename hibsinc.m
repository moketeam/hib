% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hibsinc class                                          %
% %                                                        %
% % generates sinc signals                                 %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe                                          %
% % graefe@is.mpg.de                                       %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hibsinc < hibsignal
    %Public Access Properites and Set Properties
    properties (Access = public)
        MagicNumber = 1;
    end
    methods
        function obj = set.MagicNumber(obj, MagicNumber)
            %only accept integer Magic Numbers
            obj.MagicNumber = round(MagicNumber);
        end
    end
    
    %Dependent Output Properties
    properties (Dependent = true)
        Signal
        Display
    end
    methods
        function Signal = get.Signal(obj)
            %generate constant signal
            SignalLength = obj.Sync.SampleRate / obj.Sync.BaseFrequency;
            Signal = zeros(1, SignalLength);
            PatternLength = floor(SignalLength / obj.MagicNumber);
            Pattern = zeros(1, PatternLength);
            DelayLength = round(obj.Delay * obj.Sync.SampleRate);
            SincLength = round(obj.Waveform.Length * obj.Sync.SampleRate);
            %if delay is longer than pattern everything becomes zero
            if DelayLength < PatternLength
                SincStart = DelayLength + 1;
                %if pulse is longer than pattern, it will be croped
                if (SincStart + SincLength) > PatternLength
                    SincEnd = PatternLength;
                else
                    SincEnd = SincStart + SincLength;
                end
                Period = round(obj.Sync.SampleRate / obj.Waveform.Bandwidth * 2*pi) / (2*pi);
                Phase = (SincEnd-SincStart) / 2;
                Time = 0:1:SincEnd-SincStart;
                if isempty(obj.Waveform.CenterFrequency)
                    Pattern(SincStart:SincEnd) = obj.Amplitude * sinc((Time - Phase) / Period);
                else
                    LowPeriod = round(obj.Sync.SampleRate / obj.Waveform.CenterFrequency) / (2*pi);
                    Pattern(SincStart:SincEnd) = obj.Amplitude .* sinc((Time - Phase) / Period) .* cos(Time / LowPeriod);
                end
            end
            %repeat pattern for magic number
            IdealPatternLength = SignalLength / obj.MagicNumber;
            for i=1:obj.MagicNumber
                PatternStart = round((i-1)*IdealPatternLength+1);
                PatternEnd = PatternStart+PatternLength-1;
                if PatternEnd > SignalLength
                    PatternStart = SignalLength-PatternLength;
                end
                Signal(PatternStart:PatternEnd) = Pattern;
            end
        end
        function Display = get.Display(obj)
            %display text for GUI
            Display = ['Sinc (Magic Number: ' num2str(obj.MagicNumber) ', Bandwidth: ' num2str(obj.Waveform.Bandwidth, '%10.3e') ' Hz, Length: ' num2str(obj.Waveform.Length, '%10.3e') ' s, CenterFrequency: ' num2str(obj.Waveform.CenterFrequency, '%10.3e') ' Hz, Amplitude: ' num2str(obj.Amplitude) ', Delay: ' num2str(obj.Delay, '%10.3e') ' s)'];
        end
    end
    
    %Basic methods
    methods
        function obj = hibsinc(varargin)
            %Constructor is inherited from hibsignal
            obj@hibsignal(varargin{:})
            obj.Delay = 0;
            obj.Waveform = struct('Length', [], 'CenterFrequency', [], 'Bandwidth', []);
        end
        function obj = GUI(obj)
            Input = inputdlg({'Bandwidth [Hz]', 'Sinc Length [s]:', 'Center Frequency [Hz]', 'Amplitude:', 'Magic Number:', 'Delay [s]:'}, 'HiB Sinc Signal', [1 50], {num2str(obj.Waveform.Bandwidth), num2str(obj.Waveform.Length), num2str(obj.Waveform.CenterFrequency), num2str(obj.Amplitude), num2str(obj.MagicNumber), num2str(obj.Delay)});
            if ~isempty(Input)
                obj.Waveform.Bandwidth = eval(Input{1});
                obj.Waveform.Length = eval(Input{2});
                if isempty(Input{3})
                    obj.Waveform.CenterFrequency = [];
                else
                    obj.Waveform.CenterFrequency = eval(Input{3});
                end
                obj.Amplitude = eval(Input{4});
                obj.MagicNumber = eval(Input{5});
                obj.Delay = eval(Input{6});
            end
        end
    end
end