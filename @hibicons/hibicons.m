% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % HIB Icons                                             %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gräfe (graefe@is.mpg.de)                       %
% % Felix Groß (fgross@is.mpg.de)                          %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hibicons
    %stores settings for MIEP
    
    properties %(Access = private)
        iconDir = [];
        backgroundColor = [];
    end
    
    methods (Access = public)
        %public methods including constructor and display
        
        function obj = hibicons(backgroundColor)
            %find curent icon directory
            hibDir = split(which('hibgui.m'), 'hibgui');
            obj.iconDir = fullfile(hibDir{1}, '@hibicons');
            obj.backgroundColor = backgroundColor;
        end
    end
    
    properties (Dependent)
        %Dependent properties: icons that are read transparently
        file_open
        export
        help_ex

    end
    methods
        %methods that load files
        function icon = get.file_open(obj)
            icon = obj.readIcon('file_open.png');
        end
        function icon = get.export(obj)
            icon = obj.readIcon('export.png');
        end
        function icon = get.help_ex(obj)
            icon = obj.readIcon('help_ex.png');
        end
    end
    
    methods (Access = private)
        %private support functions
        function icon = readIcon(obj, iconName)
            image = imread(fullfile(obj.iconDir, iconName), 'Background', obj.backgroundColor);
            [img, map] = rgb2ind(image, 65535);
            icon = ind2rgb(img, map);
        end
    end
end