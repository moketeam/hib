% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hibburst class                                         %
% %                                                        %
% % generates burst signals                                %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe                                          %
% % graefe@is.mpg.de                                       %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hibburst < hibsignal
    %Public Access Properites and Set Properties
    properties (Access = public)
        MagicNumber = 1;
    end
    methods
        function obj = set.MagicNumber(obj, MagicNumber)
            %only accept integer Magic Numbers
            obj.MagicNumber = round(MagicNumber);
        end
    end
    
    %Dependent Output Properties
    properties (Dependent = true)
        Signal
        Display
    end
    methods
        function Signal = get.Signal(obj)
            %generate constant signal
            SignalLength = obj.Sync.SampleRate / obj.Sync.BaseFrequency;
            Signal = zeros(1, SignalLength);
            PatternLength = floor(SignalLength / obj.MagicNumber);
            Pattern = zeros(1, PatternLength);
            DelayLength = round(obj.Delay * obj.Sync.SampleRate);
            BurstLength = round(obj.Waveform.Length * obj.Sync.SampleRate);
            %if delay is longer than pattern everything becomes zero
            if DelayLength < PatternLength
                BurstStart = DelayLength + 1;
                %if pulse is longer than pattern, it will be croped
                if (BurstStart + BurstLength) > PatternLength
                    BurstEnd = PatternLength;
                else
                    BurstEnd = BurstStart + BurstLength;
                end
                %enforce synchronizable frequency (virtual magic number)
                Frequency = round(obj.Waveform.Frequency / obj.Sync.BaseFrequency) * obj.Sync.BaseFrequency;
                Period = (obj.Sync.SampleRate / Frequency) / (2*pi);
                Phase = mod(obj.Waveform.Phase/360 * Period, Period) * (2*pi);
                Time = 0:1:BurstEnd-BurstStart;
                Pattern(BurstStart:BurstEnd) = obj.Amplitude * sin((Time + Phase) / Period);
            end
            %repeat pattern for magic number
            IdealPatternLength = SignalLength / obj.MagicNumber;
            for i=1:obj.MagicNumber
                PatternStart = round((i-1)*IdealPatternLength+1);
                PatternEnd = PatternStart+PatternLength-1;
                if PatternEnd > SignalLength
                    PatternStart = SignalLength-PatternLength;
                end
                Signal(PatternStart:PatternEnd) = Pattern;
            end
        end
        function Display = get.Display(obj)
            %display text for GUI
            Display = ['Burst (Magic Number: ' num2str(obj.MagicNumber) ', Frequency: ' num2str(round(obj.Waveform.Frequency / obj.Sync.BaseFrequency) * obj.Sync.BaseFrequency, '%10.3e') ' Hz, Length: ' num2str(obj.Waveform.Length, '%10.3e') ' s, Phase: ' num2str(obj.Waveform.Phase) '�, Amplitude: ' num2str(obj.Amplitude) ', Delay: ' num2str(obj.Delay, '%10.3e') ' s)'];
        end
    end
    
    %Basic methods
    methods
        function obj = hibburst(varargin)
            %Constructor is inherited from hibsignal
            obj@hibsignal(varargin{:})
            obj.Delay = 0;
            obj.Waveform = struct('Frequency', [], 'Length', [], 'Phase', 0);
        end
        function obj = GUI(obj)
            Input = inputdlg({'Frequency [Hz]:', 'Burst Length [s]:', 'Phase [�]', 'Amplitude:', 'Magic Number:', 'Delay [s]:'}, 'HiB Burst Signal', [1 50], {num2str(obj.Waveform.Frequency), num2str(obj.Waveform.Length), num2str(obj.Waveform.Phase), num2str(obj.Amplitude), num2str(obj.MagicNumber), num2str(obj.Delay)});
            if ~isempty(Input)
                obj.Waveform.Frequency = eval(Input{1});
                obj.Waveform.Length = eval(Input{2});
                obj.Waveform.Phase = eval(Input{3});
                obj.Amplitude = eval(Input{4});
                obj.MagicNumber = eval(Input{5});
                obj.Delay = eval(Input{6});
            end
        end
    end
end