% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % hib graphical user interface                           %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe                                          %
% % graefe@is.mpg.de                                       %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef hibgui < handle
    
    properties (Access = private)
        %stores config information
        version = 'Sep-2021';
        syncModules = {hibmaxymus, hibmaxydirect, hiblaser, hibmanual};
        outputModules = {hib_M8195A_2ch, hib_M8195A_4ch};
        hibIcons = []
    end
    
    properties
        %stores GUI handles
        fig = []; %figure handle
        tBar = []; %toolbar handle
        syncPanel = []; %sync panel handle
        syncDisplay = []; %sync display handle
        syncSelect = []; %sync selector handle
        signalPanel = []; %signal panel handle
        signalTabGroup = []; %signal tab group
        signalTabs = struct(); %stores individual channel tab handles
        signalDisplay = []; %signal display axes
        signalDisplaySelector = []; %signal display selector
        outputPanel = []; %output panel handle
        outputDownload = []; %output download button
        outputSelect = []; %output selector
    end
    
    properties (Dependent = true)
        %dependent properties
        Signal
        MagicNumber
    end
    methods
        %get methods for dependent properties
        function Signal = get.Signal(obj)
            %assemble signals for all channels
            Signal = [];
            for i=1:obj.AWG.Channels
                numSignals = size(obj.channels(i).signals, 2);
                if numSignals > 0
                    Signal(i,:) = obj.channels(i).signals{1}.Signal;
                    if numSignals > 1
                        for j=2:numSignals
                            Signal(i,:) = Signal(i,:) + obj.channels(i).signals{j}.Signal;
                        end
                    end
                else
                    %return empty signal if no signals defined for channel
                    emptySignal = hibempty(obj.Sync);
                    emptySignal.MagicNumber = min(obj.MagicNumber(obj.MagicNumber>0));
                    Signal(i,:) = emptySignal.Signal;
                end
            end
            %rescale signal to fit DAC range
            scalingFactor = max(max(abs(Signal)));
            if scalingFactor > 1
                Signal = Signal ./ scalingFactor;
            end
        end
        function MagicNumber = get.MagicNumber(obj)
            %get all magic numbers
            MagicNumber = [];
            for i=1:obj.AWG.Channels
                for j=1:size(obj.channels(i).signals, 2)
                    MagicNumber(i,j) = obj.channels(i).signals{j}.MagicNumber;
                end
            end
            if isempty(MagicNumber)
                MagicNumber = 0;
            end
        end
    end
    
    
    properties (SetAccess = private)
        %stores internal data
        Sync = []; %stores synchronisation module
        AWG = []; %stores awg module
        channels = struct(); %stores hibsignals for each channel
    end
    
    methods (Access = public)
        %public methods including constructor and display
        
        function obj = hibgui
            %main function that generates the GUI
            
            %frist, try to load old configuration from hibConfig.mat
            awgConfigFile = fullfile(getenv('APPDATA'), 'HiB', 'hibConfig.mat');
            try
                outputModules = load(awgConfigFile, 'outputModules', 'outputValue');
                obj.outputModules = outputModules.outputModules;
            catch
            end
            try
                syncModules = load(awgConfigFile, 'syncModules', 'syncValue');
                obj.syncModules = syncModules.syncModules;
            catch
            end
            
            %init first sync/output module
            obj.Sync = obj.syncModules{1};
            addlistener(obj.Sync, 'updateSync', @obj.updateSignalDisplay);
            obj.AWG = obj.outputModules{1};
            
            %init channel storage
            for i=1:obj.AWG.Channels
                obj.channels(i).signals = cell(0);
            end
            
            %determine figure position from screen size
            screenSize = get(0, 'ScreenSize');
            screenRatio = 0.8; %2/(1+sqrt(5)); %screen filling ratio
            screenSize(3) = 1920;
            screenSize(4) = 1080;
            figPos(1) = (1-screenRatio)/2*screenSize(3); %position left
            figPos(2) = (1-screenRatio)/2*screenSize(4); %position bottom
            figPos(3) = screenRatio*screenSize(3); %width
            figPos(4) = screenRatio*screenSize(4); %height
            
            %open figure
            obj.fig = figure('Position', figPos, 'Resize', 'off', 'WindowStyle', 'normal', ...
                'DockControls', 'off', 'MenuBar', 'none', 'ToolBar', 'none', ...
                'NumberTitle', 'off', 'Name', 'HiB Control', 'CloseRequestFcn', @obj.guiFileClose);
            
            
            
            
            
            %add menubar to figure
            menuFile = uimenu(obj.fig, 'Text', 'File');
            uimenu(menuFile, 'Text', 'Open', 'MenuSelectedFcn', @obj.guiFileOpen, 'Accelerator', 'O');
            uimenu(menuFile, 'Text', 'Save', 'MenuSelectedFcn', @obj.guiFileSave, 'Accelerator', 'S');
            uimenu(menuFile, 'Text', 'Close', 'MenuSelectedFcn', @obj.guiFileClose, 'Accelerator', 'X');
            menuHelp = uimenu(obj.fig, 'Text', '?');
            uimenu(menuHelp, 'Text', 'Info', 'MenuSelectedFcn', @obj.guiHelpInfo);
            
            %add toolbar to figure
            obj.tBar = uitoolbar(obj.fig);
            
            obj.hibIcons = hibicons(obj.fig.Color);
            %load open icon and add to toolbar
            uipushtool(obj.tBar, 'CData', obj.hibIcons.file_open, 'TooltipString', 'Load Folder', 'ClickedCallback', @obj.guiFileOpen);
            uipushtool(obj.tBar, 'CData', obj.hibIcons.export, 'TooltipString', 'Export Waveform', 'ClickedCallback', @obj.guiFileSave);
            uipushtool(obj.tBar, 'CData', obj.hibIcons.help_ex, 'TooltipString', 'Info', 'ClickedCallback', @obj.guiHelpInfo);
            
            %determine figure drawing area
            drawingArea = obj.fig.InnerPosition;
            
            %draw sync panel
            Pos(1) = 5; %position left
            Pos(2) = 5; % position bottom
            Pos(3) = drawingArea(3)*1/5 - 2*5; %width
            Pos(4) = drawingArea(4) - 20 - 3*5; %height
            obj.syncPanel = uipanel(obj.fig, 'Title', 'Synchronisation', ...
                'Units', 'Pixels', 'Position', Pos);
            obj.Sync.drawGUI(obj.syncPanel);
            
            %draw sync selector
            Pos(2) = Pos(2) + Pos(4) + 5; %position bottom
            Pos(4) = 20; %height
            obj.syncSelect = uicontrol(obj.fig, 'Style', 'popupmenu', ...
                'String', ' ', 'Units', 'Pixels', 'Position', Pos, ...
                'Callback', @obj.changeSync);
            for i=1:size(obj.syncModules,2)
                SyncTitles{i} = obj.syncModules{i}.Title;
            end
            obj.syncSelect.String = SyncTitles;
            
            %draw signals panel
            Pos(1) = drawingArea(3)*1/5; %position left
            Pos(2) = 5; % position bottom
            Pos(3) = drawingArea(3)*3/5; %width
            Pos(4) = drawingArea(4) - 2*5; %height
            obj.signalPanel = uipanel(obj.fig, 'Title', 'Signals', ...
                'Units', 'Pixels', 'Position', Pos);
            
            %determine panel drawing area
            panelArea = obj.signalPanel.InnerPosition;
            
            %draw signals tab
            PanelPos(1) = 5; %position left
            PanelPos(2) = 5; %position bottom
            PanelPos(3) = panelArea(3) - 2*5; %width
            PanelPos(4) = panelArea(4)*1/2 - 20 - 4*5; %height
            obj.signalTabGroup = uitabgroup(obj.signalPanel, 'Units', 'Pixels', 'Position', PanelPos);
            for i=1:obj.AWG.Channels
                obj.signalTabs(i).Tab = uitab(obj.signalTabGroup, 'Title', ['Channel ' num2str(i)], 'Units', 'Pixels');
                %determine tab drawing area
                tabArea = obj.signalTabs(i).Tab.InnerPosition;
                TabPos(1) = 5;
                TabPos(2) = 5;
                TabPos(3) = tabArea(3) - 100 - 3*5;
                TabPos(4) = tabArea(4) - 25 - 2*5;
                obj.signalTabs(i).SignalList = uicontrol(obj.signalTabs(i).Tab, ...
                    'Style', 'listbox', 'Units', 'Pixels', 'Position', TabPos);
                TabPos(1) = TabPos(3) + 2*5;
                TabPos(2) = tabArea(4) - 25 - 20 - 2*5;
                TabPos(3) = 100;
                TabPos(4) = 20;
                obj.signalTabs(i).AddSignal = uicontrol(obj.signalTabs(i).Tab, ...
                    'Style', 'pushbutton', 'String', 'Add', 'Units', 'Pixels', ...
                    'Position', TabPos, 'Callback', @obj.addSignal);
                TabPos(2) = TabPos(2) - 20 - 5;
                obj.signalTabs(i).EditSignal = uicontrol(obj.signalTabs(i).Tab, ...
                    'Style', 'pushbutton', 'String', 'Edit', 'Units', 'Pixels', ...
                    'Position', TabPos, 'Callback', @obj.editSignal);
                TabPos(2) = TabPos(2) - 20 - 5;
                obj.signalTabs(i).DeleteSignal = uicontrol(obj.signalTabs(i).Tab, ...
                    'Style', 'pushbutton', 'String', 'Delete', 'Units', 'Pixels', ...
                    'Position', TabPos, 'Callback', @obj.deleteSignal);
            end
            
            %draw signal display
            PanelPos(1) = 5; %position left
            PanelPos(2) = panelArea(4)*1/2 + 2*5; %position bottom
            PanelPos(3) = panelArea(3) - 2*5; %width
            PanelPos(4) = panelArea(4)*1/2 - 20 - 4*5; %height
            obj.signalDisplay = axes(obj.signalPanel, 'Units', 'Pixels', 'Position', PanelPos);
            obj.rescaleAxes
            
            %draw signal display type
            PanelPos(1) = 5; %position left
            PanelPos(2) = panelArea(4) - 20 - 5; %position bottom
            PanelPos(3) = panelArea(3) - 2*5; %width
            PanelPos(4) = 20; %height
            obj.signalDisplaySelector = uicontrol(obj.signalPanel, 'Style', 'popupmenu', ...
                'String', {'Time Domain' 'Frequency Domain'}, 'Units', 'Pixels', 'Position', PanelPos, ...
                'Callback', @obj.updateSignalDisplay);
            obj.updateSignalDisplay
            
            %draw download button
            Pos(1) = drawingArea(3)*4/5 + 2*5; %position left
            Pos(2) = 5; % position bottom
            Pos(3) = drawingArea(3)*1/5 - 2*5; %width
            Pos(4) = 20; %height
            obj.outputDownload = uicontrol(obj.fig, 'Style', 'pushbutton', ...
                'String', 'Download', 'Units', 'Pixels', ...
                'Position', Pos, 'Callback', @obj.downloadSignal);
            
            %draw output panel
            Pos(2) = Pos(2) + Pos(4) + 5;
            Pos(4) = drawingArea(4) - 2*20 - 5*5; %height
            obj.outputPanel = uipanel(obj.fig, 'Title', 'Output', ...
                'Units', 'Pixels', 'Position', Pos);
            obj.AWG.drawGUI(obj.outputPanel);
            
            %draw output selector
            Pos(2) = Pos(2) + Pos(4) + 5; %position bottom
            Pos(4) = 20; %height
            obj.outputSelect = uicontrol(obj.fig, 'Style', 'popupmenu', ...
                'String', ' ', 'Units', 'Pixels', 'Position', Pos, ...
                'Callback', @obj.changeOutput);
            for i=1:size(obj.outputModules,2)
                OutputTitles{i} = obj.outputModules{i}.Title;
            end
            obj.outputSelect.String = OutputTitles;
            
            %customize toolbar to show creators
            drawnow update
            jToolbar = get(get(obj.tBar,'JavaContainer'),'ComponentPeer');
            jButton = javax.swing.JButton(['Developed by Joachim Gr�fe (graefe@is.mpg.de) & Felix Gro� (fgross@is.mpg.de)| ' obj.version ' | Max Planck Institute for Intelligent Systems']);
            hButton = handle(jButton,'CallbackProperties');
            set(hButton, 'actionPerformedCallback', @obj.guiHelpInfo);
            jToolbar.add(hButton,4); % th position
            jToolbar.revalidate;
            
            %try to swap to previous sync and output configuration
            try
                obj.changeOutput('', '', outputModules.outputValue)
                obj.changeSync('', '', syncModules.syncValue)
            catch
            end
             
        end

        function delete(obj)
            %object destructor, saves AWG and Sync configuration to hibConfig.mat
            outputModules = obj.outputModules;

            awgConfigFile = fullfile(getenv('APPDATA'), 'HiB', 'hibConfig.mat');
            try
                save(awgConfigFile, 'outputModules', '-append')
            catch
                awgConfigFolder = fileparts(awgConfigFile);
                if ~exist(awgConfigFolder, 'dir')
                    mkdir(awgConfigFolder)
                end
                save(awgConfigFile, 'outputModules')
            end
            
            syncModules = obj.syncModules;
            save(awgConfigFile, 'syncModules', '-append')
        end
        
        %%GUI functionality
        %menu bar
        
        function guiFileOpen(obj, ~, ~)
            %file -> open
            [file,folder] = uigetfile({'*.hib', 'HiB Signal Definition'}, 'Save HiB Signal Definition ...');
            if ischar(file) == 0 || ischar(folder) == 0
                return
            end
            import = load(fullfile(folder,file), '-MAT');
            obj.channels = import.signalDefinition;
            obj.broadcastSync
            obj.updateSignalDisplay
        end
        
        function guiFileSave(obj, ~, ~)
            %file -> save
            [file,folder] = uiputfile({'*.hib', 'HiB Signal Definition'}, 'Save HiB Signal Definition ...');
            if ischar(file) == 0 || ischar(folder) == 0
                return
            end
            signalDefinition = obj.channels;
            save(fullfile(folder,file), 'signalDefinition');
        end
        
        function guiFileClose(obj, ~, ~)
            %file -> close
            delete(obj.fig)
            delete(obj)
        end
        
        function guiHelpInfo(obj, ~, ~)
            %? -> info
            waitfor(msgbox({['HiB Control Software Version: ' obj.version],'','Max Planck Institute for Intelligent Systems','Joachim Gr�fe & Felix Gro�'}, ...
                'HiB Control Software', 'help'))
        end
        
        %%synchronisation panel
        function changeSync(obj, ~, ~, varargin)
            
            if nargin == 4
                obj.syncSelect.Value = varargin{1};
            end
            
            %check if module was changed
            for i=1:size(obj.syncModules,2)
                if obj.syncModules{i} == obj.Sync
                    currentModule = i;
                end
            end
            
            selectedModule = obj.syncSelect.Value;
            if currentModule ~= selectedModule
                %delete old panel
                PanelPos = obj.syncPanel.Position;
                delete(obj.syncPanel);
                %init new sync module
                obj.Sync = obj.syncModules{selectedModule};
                addlistener(obj.Sync, 'updateSync', @obj.updateSignalDisplay);
                %draw sync panel
                obj.syncPanel = uipanel(obj.fig, 'Title', 'Synchronisation', ...
                    'Units', 'Pixels', 'Position', PanelPos);
                obj.Sync.drawGUI(obj.syncPanel);
                obj.broadcastSync
                obj.updateSignalDisplay
            end
            
            %adjust clock and trigger mode
            manualVal = find(strcmp(obj.syncSelect.String, 'Manual Synchronisation'));
            if obj.syncSelect.Value == manualVal
                %for manual synchonization switch to internal clock and
                %enable trigger and disable clock source
                intVal = find(strcmp(obj.AWG.outputClockSource.String, 'IntRef'));
                obj.AWG.outputClockSource.Value = intVal;
                obj.AWG.outputClockSource.Enable = 'off';
                
                obj.AWG.outputTriggerMode.Enable = 'on';
            else
                %otherwise go to external clock and disable trigger and
                %clock source
                extVal = find(strcmp(obj.AWG.outputClockSource.String, 'ExtRef'));
                obj.AWG.outputClockSource.Value = extVal;
                obj.AWG.outputClockSource.Enable = 'off';
                
                contVal = find(strcmp(obj.AWG.outputTriggerMode.String, 'Continuous'));
                obj.AWG.outputTriggerMode.Value = contVal;
                obj.AWG.outputTriggerMode.Enable = 'off';
                
            end
            
            obj.AWG.updateClockSource
            obj.AWG.updateTriggerMode
            
            syncValue = obj.syncSelect.Value;
            awgConfigFile = fullfile(getenv('APPDATA'), 'HiB', 'hibConfig.mat');
            try
                save(awgConfigFile, 'syncValue', '-append')
            catch
                awgConfigFolder = fileparts(awgConfigFile);
                if ~exist(awgConfigFolder, 'dir')
                    mkdir(awgConfigFolder)
                end
                save(awgConfigFile, 'syncValue')
            end
            
        end
        
        %%output panel
        function changeOutput(obj, ~, ~, varargin)
                        
            if nargin == 4
                obj.outputSelect.Value = varargin{1};
            end
            
            %check if module was changed
            for i=1:size(obj.outputModules,2)
                if obj.outputModules{i} == obj.AWG
                    currentModule = i;
                end
            end

            selectedModule = obj.outputSelect.Value;
            if currentModule ~= selectedModule
                %delete old panel
                PanelPos = obj.outputPanel.Position;
                delete(obj.outputPanel);
                %init new output module
                obj.AWG = obj.outputModules{selectedModule};
                %draw output panel
                obj.outputPanel = uipanel(obj.fig, 'Title', 'Output', ...
                    'Units', 'Pixels', 'Position', PanelPos);
                obj.AWG.drawGUI(obj.outputPanel);
                obj.reinitChannels
                obj.updateSignalDisplay
            end
            
            outputValue = obj.outputSelect.Value;
            
            
            awgConfigFile = fullfile(getenv('APPDATA'), 'HiB', 'hibConfig.mat');
            try
                save(awgConfigFile, 'outputValue', '-append')
            catch
                awgConfigFolder = fileparts(awgConfigFile);
                if ~exist(awgConfigFolder, 'dir')
                    mkdir(awgConfigFolder)
                end
                save(awgConfigFile, 'outputValue')
            end
        end
        
        %%channel signals
        function addSignal(obj, caller, ~)
            %add signal to channel
            %find channel selected
            channel = [];
            for i=1:size(obj.signalTabs, 2)
                if obj.signalTabs(i).AddSignal == caller
                    channel = i;
                end
            end
            if isempty(channel)
                return
            end
            %select signal type
            signalList = {'Sine', 'Pulse', 'Burst', 'Sinc', 'Constant'};
            signalIndex = listdlg('PromptString', 'Select signal type:', 'SelectionMode', 'single', 'ListString', signalList, 'Name', ['Add Signal to channel ' num2str(channel)]);
            if isempty(signalIndex)
                return
            end
            %add new signal
            newSignal = [];
            switch signalList{signalIndex}
                case 'Sine'
                    newSignal = hibsin;
                case 'Pulse'
                    newSignal = hibpulse;
                case 'Burst'
                    newSignal = hibburst;
                case 'Sinc'
                    newSignal = hibsinc;
                case 'Constant'
                    newSignal = hibconst;
            end
            if isempty(newSignal)
                return
            end
            newSignal.Sync = obj.Sync;
            newSignal = newSignal.GUI;
            %append new signal to storage
            if isempty(obj.channels(channel).signals)
                obj.channels(channel).signals{1} = newSignal;
            else
                obj.channels(channel).signals{end+1} = newSignal;
            end
            %update signal display
            obj.updateSignalDisplay
        end
        
        function editSignal(obj, caller, ~)
            %edit signal in channel
            %find channel selected
            channel = [];
            for i=1:size(obj.signalTabs, 2)
                if obj.signalTabs(i).EditSignal == caller
                    channel = i;
                end
            end
            if isempty(channel)
                return
            end
            %find signal selected
            signalIdx = obj.signalTabs(channel).SignalList.Value;
            if signalIdx > size(obj.channels(channel).signals, 2)
                return
            end
            %show GUI for editing signal
            signal = obj.channels(channel).signals{signalIdx};
            signal = signal.GUI;
            obj.channels(channel).signals{signalIdx} = signal;
            %update signal display
            obj.updateSignalDisplay
        end
        
        function deleteSignal(obj, caller, ~)
            %delete signal from channel
            %find channel selected
            channel = [];
            for i=1:size(obj.signalTabs, 2)
                if obj.signalTabs(i).DeleteSignal == caller
                    channel = i;
                end
            end
            if isempty(channel)
                return
            end
            %find signal selected
            signalIdx = obj.signalTabs(channel).SignalList.Value;
            if signalIdx > size(obj.channels(channel).signals, 2)
                return
            end
            %remove signal
            oldSignals = obj.channels(channel).signals;
            oldNumSignals = size(oldSignals,2);
            if oldNumSignals == 1
                obj.channels(channel).signals = cell(0);
            else
                if signalIdx == 1
                    newSignals = oldSignals(2:end);
                else
                    newSignals = oldSignals(1:signalIdx-1);
                end
                if signalIdx ~= oldNumSignals
                    newSignals(signalIdx:oldNumSignals-1) = oldSignals(signalIdx+1:end);
                end
                obj.channels(channel).signals = newSignals;
            end
            %adjust signallist value
            signalValue = obj.signalTabs(channel).SignalList.Value;
            if signalValue == 1
                if oldNumSignals == 1
                    signalValue = [];
                end
            else
                signalValue = signalValue - 1;
            end
            obj.signalTabs(channel).SignalList.Value = signalValue;
            %update signal display
            obj.updateSignalDisplay
        end
        
        function updateSignalDisplay(obj, ~, ~)
            %update channel display
            for channel=1:obj.AWG.Channels
                signalList = cell(0);
                for i=1:size(obj.channels(channel).signals, 2)
                    signalList{i} = obj.channels(channel).signals{i}.Display;
                end
                obj.signalTabs(channel).SignalList.String = signalList;
                if ~isempty(signalList) && isempty(obj.signalTabs(channel).SignalList.Value)
                    obj.signalTabs(channel).SignalList.Value = 1;
                end
            end
            %update signal display
            TimeDomain = flipud(rot90(obj.Signal));
            if obj.MagicNumber == 0
                TDLength = size(TimeDomain,1);
            else
                MagicNumberGreatestCommonDivider = obj.MagicNumber(1);
                for i=2:numel(obj.MagicNumber)
                    MagicNumberGreatestCommonDivider = gcd(MagicNumberGreatestCommonDivider,obj.MagicNumber(i));
                end
                TDLength = round((size(TimeDomain,1)./MagicNumberGreatestCommonDivider));
            end
            switch obj.signalDisplaySelector.Value
                case 1 %Time Domain
                    Time = (1:TDLength)./obj.Sync.SampleRate*10^9;
                    PlotTime = [0, Time];
                    
                    %old version with slightly shifted signal
                    %PlotData = [TimeDomain(TDLength,:); TimeDomain(1:TDLength,:)];
                    %new fix (hopefully)
                    PlotData = [TimeDomain(end,:); TimeDomain(end-TDLength+1:end,:)];
                    
                    %plot(obj.signalDisplay, Time, TimeDomain(1:TDLength,:))
                    plot(obj.signalDisplay, PlotTime, PlotData)
                    obj.signalDisplay.YLim = 1.05 * obj.signalDisplay.YLim;
                    obj.signalDisplay.XLim = Time(end) * [-0.01 1.01];
                    obj.signalDisplay.XLabel.String = 'Time [ns]';
                case 2 %Frequency Domain
                    SampleRate = obj.Sync.SampleRate;
                    N = pow2(nextpow2(TDLength));
                    FrequencyDomain = abs(fft(TimeDomain(1:TDLength,:),N));
                    %Divide by size of FFT to normalize to amplitude, x2
                    %because of 2-sided power spectrum (+-f)
                    FrequencyDomain = 2*FrequencyDomain./N;
                    Frequency = SampleRate*(0:(N/2))/N;
                    if ~isempty(TimeDomain)
                        plot(obj.signalDisplay, Frequency/1e9, FrequencyDomain(1:N/2+1,:))
                        obj.signalDisplay.YLim = obj.signalDisplay.YLim(2) * [-0.025 1.025];
                        obj.signalDisplay.XLim = Frequency(end)/1e9 * [-0.01 1.01];
                    else
                        plot(obj.signalDisplay, Frequency, zeros(size(Frequency,2),1))
                    end
                    obj.signalDisplay.XLabel.String = 'Frequency [GHz]';
            end
            obj.signalDisplay.XGrid = 'on';
            obj.signalDisplay.YGrid = 'on';
            obj.rescaleAxes
        end
        
        function reinitChannels(obj)
            %reinit signal storage and display, e.g. after selection of new AWG
            obj.channels = struct();
            for i=1:obj.AWG.Channels
                obj.channels(i).signals = cell(0);
            end
            
            %delete existing Tabs
            for i=1:size(obj.signalTabs,2)
                delete(obj.signalTabs(i).SignalList)
                delete(obj.signalTabs(i).AddSignal)
                delete(obj.signalTabs(i).EditSignal)
                delete(obj.signalTabs(i).DeleteSignal)
                delete(obj.signalTabs(i).Tab)
            end
            
            %determine panel drawing area
            panelArea = obj.signalPanel.InnerPosition;
            
            %draw signals tab
            PanelPos(1) = 5; %position left
            PanelPos(2) = 5; %position bottom
            PanelPos(3) = panelArea(3) - 2*5; %width
            PanelPos(4) = panelArea(4)*1/2 - 20 - 4*5; %height
            obj.signalTabGroup = uitabgroup(obj.signalPanel, 'Units', 'Pixels', 'Position', PanelPos);
            for i=1:obj.AWG.Channels
                obj.signalTabs(i).Tab = uitab(obj.signalTabGroup, 'Title', ['Channel ' num2str(i)], 'Units', 'Pixels');
                %determine tab drawing area
                tabArea = obj.signalTabs(i).Tab.InnerPosition;
                TabPos(1) = 5;
                TabPos(2) = 5;
                TabPos(3) = tabArea(3) - 100 - 3*5;
                TabPos(4) = tabArea(4) - 25 - 2*5;
                obj.signalTabs(i).SignalList = uicontrol(obj.signalTabs(i).Tab, ...
                    'Style', 'listbox', 'Units', 'Pixels', 'Position', TabPos);
                TabPos(1) = TabPos(3) + 2*5;
                TabPos(2) = tabArea(4) - 25 - 20 - 2*5;
                TabPos(3) = 100;
                TabPos(4) = 20;
                obj.signalTabs(i).AddSignal = uicontrol(obj.signalTabs(i).Tab, ...
                    'Style', 'pushbutton', 'String', 'Add', 'Units', 'Pixels', ...
                    'Position', TabPos, 'Callback', @obj.addSignal);
                TabPos(2) = TabPos(2) - 20 - 5;
                obj.signalTabs(i).EditSignal = uicontrol(obj.signalTabs(i).Tab, ...
                    'Style', 'pushbutton', 'String', 'Edit', 'Units', 'Pixels', ...
                    'Position', TabPos, 'Callback', @obj.editSignal);
                TabPos(2) = TabPos(2) - 20 - 5;
                obj.signalTabs(i).DeleteSignal = uicontrol(obj.signalTabs(i).Tab, ...
                    'Style', 'pushbutton', 'String', 'Delete', 'Units', 'Pixels', ...
                    'Position', TabPos, 'Callback', @obj.deleteSignal);
            end
        end
        
        
        %%output
        function downloadSignal(obj, ~, ~)
            %call download function of AWG object
            obj.AWG.download(obj.Signal, obj.Sync)
        end
        
        %%helper functions
        function broadcastSync(obj)
            %broadcast new sync setting
            for i=1:obj.AWG.Channels
                numSignals = size(obj.channels(i).signals, 2);
                for j=1:numSignals
                    obj.channels(i).signals{j}.Sync = obj.Sync;
                end
            end
        end
        
        function rescaleAxes(obj)
            %rescale axes for tight inset
            panelArea = obj.signalPanel.InnerPosition;
            AxesPosition(1) = 5; %position left
            AxesPosition(2) = panelArea(4)*1/2 + 2*5; %position bottom
            AxesPosition(3) = panelArea(3) - 2*5; %width
            AxesPosition(4) = panelArea(4)*1/2 - 20 - 4*5; %height
            AxesInset = obj.signalDisplay.TightInset;
            AxesPosition(1) = AxesPosition(1) + AxesInset(1);
            AxesPosition(2) = AxesPosition(2) + AxesInset(2);
            AxesPosition(3) = AxesPosition(3) - AxesInset(1) - AxesInset(3);
            AxesPosition(4) = AxesPosition(4) - AxesInset(2) - AxesInset(4);
            obj.signalDisplay.Position = AxesPosition;
        end
        
    end
end
