% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % HiB AWG Control                                        %
% %                                                        %
% % Max Planck Institute for Intelligent Systems           %
% % Joachim Gr�fe, Felix Gro�                              %
% % graefe@is.mpg.de, fgross@is.mpg.de                     %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function hib2
    myHiB = hibgui;
    try
        %waitfor(myHiB)
        myHiB
    catch errHiB
        disp(errHiB)
        delete(myHiB)
    end
end